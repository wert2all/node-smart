/*jslint browser: true*/
/*global define:true */
define(['jquery', 'dom', 'utils/dialog', 'utils/form'], ($, dom, dialog, form) => {
    'use strict';
    const _options = {
            elements: []
        },
        buttonSelector = "addbutton",
        removeButton = () => {
            $("#" + buttonSelector).remove();
        },
        createButton = () => {
            return dom.create('button')
                .css(
                    ['mdl-button', 'mdl-js-ripple-effect', 'mdl-js-button', 'mdl-button--fab', 'mdl-color--accent']
                )
                .append(
                    dom.create('i')
                        .css(
                            ['material-icons', 'mdl-color-text--white']
                        )
                        .text('add')
                        .build()
                )
                .append(
                    dom.create('span')
                        .css(['visually', 'hidden'])
                        .text('add')
                        .build()
                )
                .append(
                    dom.create('span')
                        .css('mdl-button__ripple-container')
                        .append(
                            dom.create('span')
                                .css('mdl-ripple')
                                .build()
                        )
                        .build()
                )
                .event('click', () => {
                    const list = $("#" + buttonSelector).find('.mdl-list');
                    if (list.hasClass('hidden')) {
                        list.removeClass('hidden');
                    } else {
                        list.addClass('hidden');
                    }
                })
                .build();
        },
        _createDeviceForm = (types) => {
            let formObject = form.create('addDevice', '#')
                .addTextField('deviceName', 'deviceName');
            types.forEach((type) => {
                formObject.addRadio('deviceType', 'device_' + type, type);
            });

            return formObject;
        },
        createList = () => {
            let list = dom.create('ul').css(['mdl-list', 'visually', 'hidden']);
            const form = _createDeviceForm(_options.elements)
                    .onSubmit((event) => {
                        console.log(event);
                        return false;
                    })
                    .build()
                    .build(),
                dialogObj = dialog.create('addDeviceDialog', {
                    onOk: () => {
                        form.submit();
                    }
                })
                    .title("Dialog test")
                    .content(form);

            _options.elements.forEach(type => {
                list.append(
                    dom.create('li')
                        .css('mdl-list__item')
                        .append(
                            dom.create('a')
                                .css('mdl-list__item-primary-content')
                                .attr('href', "#")
                                .append(
                                    dom.create('i')
                                        .css('material-icons mdl-list__item-icon')
                                        .text('person')
                                        .build()
                                )
                                .text(type)
                                .event('click', () => {
                                    dialogObj
                                        .getContent()
                                        .querySelectorAll('.device_radio_container input.mdl-radio__button')
                                        .forEach(node => {
                                            node.setAttribute('checked', node.getAttribute("value") == type);
                                        });
                                    dialogObj.title("Dialog " + type)
                                        .show();
                                })
                                .build()
                        )
                        .build()
                );
            });
            list = list.build();
            componentHandler.upgradeElement(list);
            return list;
        },
        createModuleContent = () => {
            let _div = dom.create('div')
                .attr('id', buttonSelector)
                .append(createButton())
                .append(
                    dom.create('div')
                        .css('clearboth')
                        .build()
                )
                .append(createList())
                .append(
                    dom.create('div')
                        .css('clearboth')
                        .build()
                )
                .build();

            componentHandler.upgradeElement(_div);
            $("body").prepend(_div);
        };

    let _socket = null,
        _log = null;

    return {
        init: (options, global) => {
            _options.elements = options;
            _socket = global.socket;
        },
        run: () => {
            removeButton();
            createModuleContent();
            _options.elements.forEach(item => {
                /*global console:true */
                console.log(item);
            });
        }
    };
});
