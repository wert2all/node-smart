/*jslint browser: true*/
/*global define:true */
define(['../utils/log'], log => {
    'use strict';
    let _options = {
            voice: null,
            error: "Can't set up Speech Synthesis API",
            rate: 1,
            pitch: 1,
            volume: 1,
            callback: {
                onvoices: Function.prototype,
                onsaid: Function.prototype
            }
        },
        _user = {
            lang: "en-US",
            say: []
        },
        _currentVoice = false,
        speechSynthesis;

    function _say(text) {
        if (_currentVoice !== false) {
            // Create the utterance object setting the chosen parameters
            /*global SpeechSynthesisUtterance:true */
            let utterance = new SpeechSynthesisUtterance();
            utterance.text = text;
            utterance.voice = _currentVoice;
            utterance.lang = _currentVoice.lang;
            utterance.rate = _options.rate;
            utterance.pitch = _options.pitch;
            utterance.volume = _options.volume;
            speechSynthesis.speak(utterance);
            _options.callback.onsaid(text, _currentVoice);
        }
    }

    function _applyVoices(voices) {
        if (voices.length !== 0) {
            voices.forEach(voice => {
                if (voice.voice) {
                    _options.voice = voice;
                }
                if (voice.lang === _user.lang) {
                    _currentVoice = voice;
                }
            });
            if (_currentVoice === false) {
                _currentVoice = _options.voice;
            }

            if (_user.say.length !== 0) {
                _user.say.map(text => {
                    _say(text);
                });
                _user.say = [];
            }
            _options.callback.onvoices(voices, _currentVoice);
        }
    }

    if (!window.hasOwnProperty('SpeechSynthesisUtterance')) {
        log.add(_options.error, Function.prototype);
    } else {
        speechSynthesis = window.speechSynthesis;
        speechSynthesis.addEventListener('voiceschanged', function onVoiceChanged() {
            speechSynthesis.removeEventListener('voiceschanged', onVoiceChanged);
            _applyVoices(speechSynthesis.getVoices());
        });

        _applyVoices(speechSynthesis.getVoices());
    }

    return {
        init: options => {
            if (options.lang) {
                _user.lang = options.lang;
            }
            if (options && typeof options.volume !== "undefined") {
                _options.volume = options.volume;
            }
            if (options.onvoices && typeof options.onvoices === "function") {
                _options.callback.onvoices = options.onvoices;
            }
            if (options.onsaid && typeof options.onsaid === "function") {
                _options.callback.onsaid = options.onsaid;
            }
        },
        run: () => {

        },
        say: function (text) {
            if (speechSynthesis === null) {
                log.add(_options.error, Function.prototype);
            } else {
                if (_currentVoice === false) {
                    _user.say.push(text);
                } else {
                    _say(text);
                }
            }
        }
    };
});
