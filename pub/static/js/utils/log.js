/*jslint browser: true*/
/*global define:true */
define(['jquery', 'dom'], function ($, dom) {
    "use strict";
    /*global MutationObserver:true */
    let scrollContainer = document.getElementById("logContent"),
        observer = new MutationObserver(() => {
            $(scrollContainer).animate({
                scrollTop: scrollContainer.scrollHeight
            }, 800);

            // scrollContainer.scrollTop = scrollContainer.scrollHeight;
        }),
        _defaultCallback = () => {

        };
    observer.observe(scrollContainer, {
        childList: true
    });

    function _add(text, className, func) {
        scrollContainer.appendChild(
            dom.create('p')
                .css(className)
                .text(text)
                .build()
        );
        (() => {
            if (typeof func !== 'function') {
                _defaultCallback(text);
            } else {
                func(text);
            }
        })(text, func);
    }

    return {
        add: (text, func) => {
            _add(text, 'notice', func);
        },
        setDefaultCallback: func => {
            if (typeof func === 'function') {
                _defaultCallback = func;
            }
        },
        notice: (text, func) => {
            _add(text, 'notice', func);
        },
        error: (text, func) => {
            _add(text, 'error', func);
        },
        warning: (text, func) => {
            _add(text, 'warning', func);
        }
    };
});
