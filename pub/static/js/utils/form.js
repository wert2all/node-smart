"use strict";
define(['dom'], (dom) => {

    const Elements = function (name) {
            this._name = name;
        },
        Submit = function (name) {
            this._name = name;
        },
        TextField = function (name) {
            this._name = name;
        },
        DomElement = function (name) {
            this._name = name;
        },
        Radio = function (name) {
            this._name = name;
        };

    Elements.prototype.validate = function () {
        return false;
    };
    Elements.prototype.getDom = function () {

    };

    Submit.prototype.__proto__ = Elements.prototype;
    TextField.prototype.__proto__ = Elements.prototype;
    DomElement.prototype.__proto__ = Elements.prototype;
    Radio.prototype.__proto__ = Elements.prototype;

    return {
        create: (name, action) => {
            const _options = {
                name: "form",
                action: "#",
                onSubmit: () => {
                    return true;
                },
                elements: [],
                newElements: []
            };
            _options.name = name;
            _options.action = action;

            let _return = {
                onSubmit: (func = Function.prototype) => {
                    _options.onSubmit = func;
                    return _return;
                },
                addTextField: (name, text) => {
                    _options.elements.push({
                        type: "textfield",
                        name: name,
                        text: text
                    });

                    return _return;
                },
                addSubmit: (buttonName) => {
                    _options.elements.push({
                        type: "submit",
                        name: 'submit',
                        text: buttonName
                    });
                    return _return;
                },
                addDomContent: (domElement) => {
                    _options.elements.push({
                        type: "dom",
                        name: 'dom',
                        text: domElement.build()
                    });
                    return _return;
                },
                addRadio: (name, value, text) => {
                    _options.elements.push({
                        type: "radio",
                        name: name,
                        text: text,
                        value: value
                    });
                    return _return;
                },
                build: () => {
                    let _form = dom.create('form')
                        .attr('name', _options.name)
                        .attr('action', _options.action)
                        .event('submit', _options.onSubmit);
                    _options.elements.forEach(item => {
                        switch (item.type) {
                            case "textfield":
                                _form.append(
                                    dom.create('div')
                                        .css('mdl-textfield')
                                        .css('mdl-js-textfield')
                                        .css('mdl-textfield--floating-label')
                                        .append(
                                            dom.create('input')
                                                .css('mdl-textfield__input')
                                                .attr('type', 'text')
                                                .attr('name', item.name)
                                                .attr('id', item.name)
                                                .build()
                                        )
                                        .append(
                                            dom.create('label')
                                                .css('mdl-textfield__label')
                                                .attr('for', item.name)
                                                .text(item.text)
                                                .build()
                                        )
                                        .build()
                                );
                                break;
                            case 'submit':
                                _form.append(
                                    dom.create('button')
                                        .css('mdl-button')
                                        .css('mdl-js-button')
                                        .css('mdl-button--raised')
                                        .css('mdl-js-ripple-effect')
                                        .css('mdl-button--accent')
                                        .text(item.text)
                                        .attr('id', item.name)
                                        .attr('name', item.name)
                                        .event('submit', this.onSubmit)
                                        .build()
                                );
                                break;
                            case 'dom':
                                _form.append(item.text);
                                break;
                            case 'radio':
                                _form.append(
                                    dom.create('label')
                                        .css('mdl-radio')
                                        .css('mdl-js-radio')
                                        .css('mdl-js-ripple-effect')
                                        .attr('for', item.value)
                                        .append(
                                            dom.create('input')
                                                .attr('type', 'radio')
                                                .attr('id', item.value)
                                                .attr('value', item.value)
                                                .attr('name', item.name)
                                                .css('mdl-radio__button')
                                                .build()
                                        )
                                        .append(
                                            dom.create('span')
                                                .text(item.text)
                                                .css('mdl-radio__label')
                                                .build()
                                        )
                                        .build()
                                );

                                break;
                        }
                    });
                    return _form;
                },
                add: (element) => {
                    _options.newElements.push(element);
                    return _return;
                },
                makeElement: (type, name) => {
                    switch (type) {
                        case 'submit':
                            return new Submit(name);
                        case  'textfield':
                            return new TextField(name);
                        case 'dom':
                            return new DomElement(name);
                        case 'radio':
                            return new Radio(name);
                    }
                },
                buildForm: () => {
                    let _form = dom.create('form')
                        .attr('name', _options.name)
                        .attr('action', _options.action)
                        .event('submit', _options.onSubmit);
                    _options.newElements.forEach(item => {
                        _form.append(item.build()
                            .build()
                        )
                    });
                    return {
                        validate: Function.prototype,
                        getDom: () => {
                            return _form;
                        }
                    };
                }
            };
            return _return;
        }
    }
});
