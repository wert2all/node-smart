/*jslint browser: true*/
/*global define:true */
define(function () {
    "use strict";
    return {
        process: (message, global) => {
            message.forEach(item => {
                console.log(item);
                if (item.action) {
                    switch (item.action) {
                        case "loadmodule": {
                            if (item.options.name) {
                                require(["module/" + item.options.name], module => {
                                    module.init(item.options.values, global);
                                    module.run();
                                });
                            }
                            break;
                        }
                        case "log":
                            if (item.options.type && item.options.text) {
                                let text = item.options.text,
                                    type = item.options.type;
                                switch (type) {
                                    case "error":
                                        global.log.error(text);
                                        break;
                                    case "warning":
                                        global.log.warning(text);
                                        break;
                                    default:
                                        global.log.notice(text);
                                }
                            }
                            break;
                    }
                }
            });
        }
    };
});
