define(['jquery', 'dom'], ($, dom) => {
    "use strict";
    return {
        create: (dialogKey, options) => {
            const _initTitle = () => {
                    _title = _dialog.querySelector("h4.mdl-dialog__title");
                },
                _initContent = () => {
                    _content = _dialog.querySelector("div.mdl-dialog__content");
                },
                _initDialog = (dialogKey) => {
                    _dialog = document.querySelector("#" + dialogKey);
                    _initTitle();
                    _initContent();
                },
                _createTitle = (text) => {
                    return dom.create('h4')
                        .css('mdl-dialog__title')
                        .text(text)
                        .build();
                },
                _createContent = (content) => {
                    return dom.create('div')
                        .css('mdl-dialog__content')
                        .append(content)
                        .build();
                },
                createDialog = (dialogKey) => {
                    $('body').append(
                        dom.create('dialog')
                            .css('mdl-dialog')
                            .attr('id', dialogKey)
                            .append(_createTitle(_option.title))
                            .append(_createContent(_option.content))
                            .append(
                                dom.create('div')
                                    .css('mdl-dialog__actions')
                                    .append(
                                        dom.create('button')
                                            .attr('type', 'button')
                                            .css('mdl-button')
                                            .text(_option.buttons.ok.text)
                                            .event('click', _option.buttons.ok.click)
                                            .build()
                                    )
                                    .append(
                                        dom.create('button')
                                            .attr('type', 'button')
                                            .css('mdl-button')
                                            .css('close')
                                            .text(_option.buttons.cancel.text)
                                            .event('click', _option.buttons.cancel.click)
                                            .build()
                                    )
                                    .build()
                            )
                            .build()
                    );
                };

            let _dialog = null,
                _title = null,
                _content = null,
                _return = {
                    show: () => {
                        if (!_dialog.showModal) {
                            window.dialogPolyfill.registerDialog(_dialog);
                        }
                        _dialog.showModal();
                    },
                    title: (title) => {
                        dom.replace(_title, _createTitle(title));
                        _initTitle();
                        return _return;
                    },
                    content: (content) => {
                        _initContent();
                        dom.replace(_content, _createContent(content));
                        _initContent();
                        return _return;
                    },
                    close: () => {
                        _dialog.close();
                    },
                    getDialog: () => {
                        return _dialog;
                    },
                    getContent: () => {
                        return _content;
                    }
                },
                _option = {
                    title: "Dialog",
                    content: document.createTextNode("Content"),
                    buttons: {
                        ok: {
                            text: 'Ok   ',
                            click: Function.prototype
                        },
                        cancel: {
                            text: "Cancel",
                            click: () => {
                                _dialog.close();
                            }
                        }
                    }
                };

            if (options && typeof options.onOk === "function") {
                _option.buttons.ok.click = options.onOk;
            }
            if (document.querySelector("#" + dialogKey) === null) {
                createDialog(dialogKey);
            }

            _initDialog(dialogKey);

            return _return;
        }
    };
});
