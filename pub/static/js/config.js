requirejs.config({
    baseUrl: "static/js/",
    paths: {
        "jquery": 'lib/jquery.min',
        "material": "lib/material.min",
        "dom": "lib/dom",
        socketio: '../../../socket.io/socket.io'
    },
    shim: {
        'socketio': {
            exports: 'io'
        }
    }
});

require(['utils/log', 'module/speak'], (log, speak) => {
    speak.init({
        onvoices: voices => {
            // console.log("Voices: ");
            voices.map(function (voice) {
                // console.log("   " + voice.name + "[" + voice.lang + "]");
            });
        },
        volume: 0,
        // lang:"ru-RU"
    });
    log.setDefaultCallback(data => {
        speak.say(data);
    });
    log.add("Hello! My name is Mashka. Let's burn this house to fucking hell!");
    log.add('Connecting to web-socket server.');
    require(['socketio'], io => {
        let socket = io();
        log.add('Connected!');
        socket
            .on('message', message => {
                require(["utils/message"], mes => {
                    mes.process(message, {
                        "socket": socket,
                        "log": log
                    });
                });
            })
            .on('error', error => {
                log.add(error.message);
            });
        socket.emit('message', {
            text: 'test message'
        });
    });
    require(['material'], () => {
        Array.prototype.forEach.call(document.querySelectorAll('.mdl-card__media'), el => {
            let link = el.querySelector('a');
            if (!link) {
                return;
            }
            let target = link.getAttribute('href');
            if (!target) {
                return;
            }
            el.addEventListener('click', () => {
                location.href = target;
            });
        });
    });
});
