const gulp = require('gulp'),
    jscs = require('gulp-jscs'),
    _jsList = [
        './modules/*.js',
        './pub/static/js/module/*.js',
        './pub/static/js/utils/*.js'
    ];

gulp.task('default', ['test']);

gulp.task('test', ['JSCS']);

gulp.task('JSCS', () => gulp.src(_jsList)
    .pipe(jscs({}))
    .pipe(jscs.reporter())
    .pipe(jscs.reporter('fail')));
