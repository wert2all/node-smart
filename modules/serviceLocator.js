/*jslint node: true */

"use strict";
/* global module:true */
class Services {
    constructor() {
        this.list = {}
    }

    add(key, requireValue, params, customInit = null) {
        this.list[key] = {
            require: requireValue,
            params: params,
            customInit: customInit
        }
    }

    hasService(key) {
        return this.list.hasOwnProperty(key);
    }

    get(service) {
        return this.list[service];
    }
}

module.exports = class {
    constructor() {

        this.cache = {};

        this.services = new Services();
        this.services.add('CONFIG', 'node-config-reader', [__dirname + "/../"]);
        this.services.add('LOG', './message/log', [this]);
        this.services.add('APP', './app/app', []);
        this.services.add('WebSockets', 'socket.io', [this.get('APP').getHttp()]);

        this.services.add('DB', './db/model/fabric', () => {
            return [
                new (require('sequelize'))(
                    this.get('CONFIG').get().mysql.url
                )
            ]
        });

        this.services.add('MQ', './queue', [
            {
                "server": this.get('CONFIG').get().mq
            }
        ]);

        this.services.add('SESSION', "", [], () => {
            const session = require('express-session');
            return session({
                store: new require('session-file-store')(session)({
                    path: __dirname + '/sessions'
                }),
                resave: false,
                saveUninitialized: true,
                secret: this.get('CONFIG').get().server.sessions.secret
            });
        });
    }

    get(service) {
        const _initClass = (serviceParams) => {
            if (serviceParams.customInit !== null) {
                return serviceParams.customInit(serviceParams);
            }

            let params = [];
            if (typeof serviceParams.params == "function") {
                params = serviceParams.params();
            } else {
                params = serviceParams.params;
            }

            return new (require(serviceParams.require))(...params);
        };
        if (!this.cache.hasOwnProperty(service)) {
            if (this.services.hasService(service)) {
                this.cache[service] = _initClass(this.services.get(service));
            }
        }
        return this.cache[service];
    }

    addService(serviceName, service) {
        this.cache[serviceName] = service;
    }
};
