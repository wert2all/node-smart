/*jslint node: true */

"use strict";
/* global module:true */
module.exports = class {
    constructor() {
        let express = require('express'),
            bodyParser = require('body-parser'),
            app = express(),
            http = require('http');

        app.use(bodyParser.json());       // to support JSON-encoded bodies
        app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
            extended: true
        }));

        this.http = http.createServer(app).listen(3000);
        this.app = app;
    }

    getApp() {
        return this.app;
    }

    getHttp() {
        return this.http;
    }
};
