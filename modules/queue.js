/*jslint node: true */

"use strict";
/* global module:true */
module.exports = class {

    constructor(options) {
        this._queue = [];
        this._options = {
            server: {
                url: "amqp://guest:guest@localhost:5672",
                exchange: "smart",
                queue: "messages"
            },
            subscriber: null,
            isCanSend: false,
            exchange: null,
            queue: null
        };
        if (options && options.onMessage && typeof options.onMessage === 'function') {
            this._options.onMessage = options.onMessage;
        }
        if (options && options.server) {
            this._options.server = require('util')._extend(this._options.server, options.server);
        }

        if (options && options.subscriber && typeof options.subscriber === 'function') {
            this._options.subscriber = options.subscriber;
        }

        this._trySubscribe = () => {
            if (this._options.subscriber !== null && this._options.queue !== null) {
                this._options.queue.subscribe((message) => {
                    this._options.subscriber(JSON.parse(message.data));
                });
            }
        };

        this._sendMessage = data => {
            this._options.exchange.publish('', JSON.stringify(data), {});
        };

        const self = this;

        const connection = require('amqp')
            .createConnection(this._options.server)
            .on('error', e => {
                console.trace(e);
            })
            .on('ready', () => {
                console.log('connected to RabbitMQ');
                connection.exchange(self._options.server.exchange, {}, exchange => {
                    console.log(`Exchange ${exchange.name} is open`);

                    connection.queue(self._options.server.queue, queue => {

                        self._options.queue = queue;
                        self._options.queue.bind(exchange, '');

                        self._trySubscribe();

                        self._options.exchange = exchange;
                        self._options.isCanSend = true;
                        if (self._queue.length !== 0) {
                            self._queue.map(data => {
                                self._sendMessage(data);
                            });
                            self._queue = [];
                        }

                    });
                });
            });
    }

    send(data) {
        if (this._options.isCanSend) {
            this._sendMessage(data);
        } else {
            this._queue.push(data);
        }
    }

    setSubscriber(func = Function.prototype) {
        this._options.subscriber = func;
        this._trySubscribe();
    }

};
