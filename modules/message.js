/*jslint node: true */

"use strict";

/* global module:true */
module.exports = class {

    constructor(action) {
        this._adv = {};
        this._action = action;
    }

    getData() {
        return {
            action: this._action,
            options: this._adv
        }
    }

    add(key, value) {
        this._adv[key] = value;
        return this;
    }
};
