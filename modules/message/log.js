/*jslint node: true */
"use strict";

module.exports = class {
    constructor(serviceLocator) {
        this._messageClass = require('./../message');
        this.serviceLocator = serviceLocator;
    }

    send(type, messageText) {
        let message = new this._messageClass('log');
        message.add('text', messageText);
        message.add('type', type);

        const socket = this.serviceLocator.get('socket');
        if (socket !== null) {
            socket.emit('message', [message.getData()]);
        }
    }

    error(messageText) {
        this.send('error', messageText);
    }

    warning(messageText) {
        this.send('warning', messageText);
    }
};