/*jslint node: true */
"use strict";

module.exports = class {
    constructor(sequelize) {
        let Sequelize = require('sequelize');
        this.logger = null;
        this._event = sequelize.define('event', {
            time: Sequelize.DATE,
            data: Sequelize.STRING
        });
        this._event.error = (err) => {
            if (this.logger !== null) {
                this.logger.error(err.message);
            }
        }
    }

    setLogger(logger) {
        this.logger = logger;
        return this;
    }

    get(model) {
        switch (model) {
            case "event":
                return new (class {
                    constructor(model) {
                        this.model = model;
                    }

                    add(message) {
                        this.model
                            .sync()
                            .then(event => {
                                event.create({
                                    data: message
                                });
                            })
                            .catch(this.model.error);
                        return this;
                    }
                })(this._event);
        }
    }
};
