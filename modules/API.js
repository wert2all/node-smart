/*jslint node: true */
"use strict";

module.exports = class {
    constructor(apiRouter, serviceLocator) {
        apiRouter.use((req, res, next) => {
            next(); // make sure we go to the next routes and don't stop here
        });

        apiRouter.get('/', (req, res) => {
            res.json({
                error: false
            });
        });

        apiRouter.post('/event', (req, res) => {
            let _response = {
                error: false,
                message: ''
            };

            const postData = {
                device: parseInt(req.body.device, 10),
                event: parseInt(req.body.event, 10),
                value: req.body.value
            };
            for (let param in postData) {
                if (isNaN(postData[param])) {
                    _response.error = true;
                    _response.message = `Bad request: ${param} is NaN`;
                }
            }
            if (_response.error === false) {
                if (postData.device === 0) {
                    _response.error = true;
                    _response.message = "Bad request";
                }
                if (postData.event === 0) {
                    _response.error = true;
                    _response.message = "Bad request";
                }
            }
            if (_response.error !== true) {
                postData.action = 'event';
                _response.message = "Done";
                serviceLocator.get('MQ').send([postData]);
            }
            res.json(_response);
        });

        this.router = apiRouter;
    }

    get() {
        return this.router;
    }
};
