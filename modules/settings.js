/*jslint node: true */

"use strict";

module.exports = class {
    constructor(MessageClass) {
        this.msgObj = new MessageClass("loadmodule");
        this.msgObj
            .add('name', 'addButton')
            .add('values', [
                'desktop', 'phone'
            ]);
    }

    message() {
        return [
            this.msgObj.getData()
        ];
    }
};
