"use strict";
const Config = require(__dirname + '/../modules/config');

const config = new Config(__dirname + "/../").load(),
    mq = require('../modules/queue')({
        "server": config.mq
    });

process.openStdin()
    .addListener("data", d => {
        function _createOther(data) {
            try {
                data = JSON.parse(data);
            } catch (ex) {
                data = {
                    "text": data
                }
            }
            return data;
        }

        try {
            let splited = (new RegExp('^(([a-zA-Z]+) )(.*)')).exec(
                d.toString().trim()
            );
            if (splited !== null) {
                let other = _createOther(splited[3]);
                other.action = splited[2];
                mq.send([
                    other
                ]);
            } else {
                throw "Bad action.";
            }
        } catch (ex) {
            console.log(ex);
        }
    });
