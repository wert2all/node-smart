"use strict";
const Settings = require('./modules/settings'),
    serviceLocator = new ( require('./modules/serviceLocator'))();

const dbFabric = serviceLocator.get('DB'),
    express = require('express'),
    session = serviceLocator.get('SESSION'),
    mq = serviceLocator.get('MQ')
        .setSubscriber((message) => {
            console.log("need to control messages");
            console.log(message);
        });

serviceLocator.get('WebSockets')
    .use((socket, next) => {
        session(socket.request, socket.request.res, next);
    })
    .on('connection', function (socket) {
        serviceLocator.addService('socket', socket);
        console.log('a user connected');
        // console.log( socket.request.session)
        socket.emit(
            'message',
            new Settings(require('./modules/message'))
                .message()
        );
        socket.on('message', function (message) {
            mq.send(message);
            dbFabric.get('event').add(JSON.stringify(message));
        });
        socket.on('disconnect', function () {
            console.log('user disconnected');
        });

        dbFabric.setLogger(serviceLocator.get('LOG'));

    });

let app = serviceLocator.get('APP').getApp();
app.use('/static', express.static(__dirname + '/pub/static/'));
app.use(session);
app.use(
    '/api/:version',
    new (require('./modules/API'))(express.Router(), mq).get()
);
app.get('/', (req, res) => {
    console.log(req.session);
    res.sendFile(__dirname + '/pub/index.html');
});
