"use strict";
const App = require('./modules/app/app'),
    API = require('./modules/API'),
    express = require('express'),
    serviceLocator = new ( require('./modules/serviceLocator'))();

new App()
    .getApp()
    .use(
        '/api/:version',
        new API(express.Router(), serviceLocator).get()
    );
